# -*- coding:utf-8 -*-                                                                                                                   
'''
http://qiita.com/Salinger/items/020b670466a9835c94bb
http://aidiary.hatenablog.com/entry/20101121/1290339360
http://hivecolor.com/id/86
'''
# from __future__ import print_function

import sys
from pyspark import SparkContext
from pyspark.sql import SQLContext
from requests_oauthlib import OAuth1Session
import json
import MeCab
import pandas as pd

dict = {}
                    
### Functions

def popCount(sc):

    # 列名の設定
    names = ['query', 'popular']

    # 列の値の型を設定
    dtype = {'query':'object','popular':'int64'}

    # pandasを用いてCSVを読み込む
    pdf = pd.read_csv('./popular_twitter.csv', header=None, names=names, dtype=dtype, encoding="utf-8-sig")
    print(pdf)

    # Sparkの各種設定
    # conf = SparkConf().setMaster("local").setAppName("Pandas PySpark App")
    # sc = SparkContext(conf = conf)
    sqlContext = SQLContext(sc)

    # pandasのDataFrameからSparkのDataFrameに変換
    sdf = sqlContext.createDataFrame(pdf)

    print(sdf.collect())
    sdfob = sdf.orderBy(['popular'], ascending=False)

    for sss in sdfob.collect():
      sys.stdout.write('(' + sss[0].encode('utf-8') + ", " + str(sss[1]) + ")\n")

    return

def wordCount(search, sc):
    # Load and parse the data
    text_file = sc.textFile("./noun.txt", use_unicode=True)

    tffm = text_file.flatMap(lambda line: line.split(" "))
    
    '''
    for s in tffm.collect():
        sys.stdout.write(s.encode('utf-8') + " ")
    print("\n")
    '''

    tffmm = tffm.map(lambda word: (word, 1))

    # print(tffmm.take(100))
    # print("\n")


    ''' 
    for s in tffmm.collect():
       sys.stdout.write('(' + s[0].encode('utf-8') + ", " + str(s[1]) + ") ")
    print("\n")
    '''

    counts = tffmm.reduceByKey(lambda a, b: a + b).sortBy(lambda (word, count): count, ascending=False) 
    # print(counts.take(100))
    # print("\n")

    sum = 0
    for s in counts.collect():
       sum += s[1]
       sys.stdout.write('(' + s[0].encode('utf-8') + ", " + str(s[1]) + ") ")
    print("\n")
    
    dict[search] = sum

    # Save
    # try:
    #     counts.saveAsTextFile("./outputCount")
    # except Exception, e:
    #     print('FileAlreadyExistsException')
 
    f = open('result.txt','w')
    for s in counts.collect():
       f.write('(' + s[0].encode('utf-8') + ", " + str(s[1]) + ")\n")
    f.close()
    return                                                                                                                                                     
def extractKeyword(text):
    """textを形態素解析して、名詞のみのリストを返す"""
    tagger = MeCab.Tagger('-Ochasen')
    node = tagger.parseToNode(text.encode('utf-8'))
    keywords = []
    while node:
        if node.feature.split(",")[0] == "名詞":
            keywords.append(node.surface)
        node = node.next
    return keywords

def main(search):
    tweets = tweet_search(search)
    ft = open('tweet.txt', 'w')
    fn = open('noun.txt', 'w')

    for tweet in tweets["statuses"]:
        tweet_id = tweet[u'id_str']
        text = tweet[u'text']
        created_at = tweet[u'created_at']
        user_id = tweet[u'user'][u'id_str']
        # user_description = tweet[u'user'][u'description']
        # screen_name = tweet[u'user'][u'screen_name']
        # user_name = tweet[u'user'][u'name']
        # print "tweet_id:", tweet_id
        # print "text:", text
        # print "created_at:", created_at
        # print "user_id:", user_id
        # print "user_desc:", user_description
        # print "screen_name:", screen_name
        # print "user_name:", user_name
        ft.write('* ' + text.encode('utf-8') + "\n")
        keywords = extractKeyword(text)
        for w in keywords:
           fn.write(w + ' ')
           # print(type(w)) 
           # print w
    
    ft.close()
    fn.close()        
    return

def create_oath_session():
    with open('config.json') as config_data:
        config = json.load(config_data)
    oath = OAuth1Session(
        config["consumer_key"],
        config["consumer_secret"],
        config["access_token"],
        config["access_token_secret"]
    )
    return oath

def tweet_search(search_word):
    url = "https://api.twitter.com/1.1/search/tweets.json?"
    params = {
        "q": unicode(search_word),
        "lang": "ja",
        "result_type": "recent",
        "count": "15"
        }
    oath = create_oath_session()
    responce = oath.get(url, params = params)
    if responce.status_code != 200:
        print "Error code: %d" %(responce.status_code)
        return None
    tweets = json.loads(responce.text)
    return tweets


### Execute                                                                                                                                                       
if __name__ == "__main__":
    f = open('query.txt', 'r')
    sc = SparkContext(appName="PythonWordCountExample")
    search = []
    for line in f:
        search.append(line.replace("\n", "").decode('utf-8'))
    
    for s in search:
        main(s)
        wordCount(s, sc)
    f.close()
    # print dict

    ft = open('popular_twitter.csv', 'w')
    for key, value in dict.iteritems() :
       ft.write(key.encode('utf-8') + ', ' + str(value) + '\n')
    ft.close()
    popCount(sc)   
